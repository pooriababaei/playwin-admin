import {
    Labeled,
    List,
    SimpleShowLayout,
    Show,
    Datagrid,
    TextField,
    ImageField,
    DateField,
    NumberField,
    BooleanField,
    FileField,
    TabbedForm,
    FormTab,
    ReferenceField,
    EditButton,
    Edit,
    Create,
    SimpleForm,
    RichTextField,
    DisabledInput,
    SimpleFormIterator,
    ReferenceInput,
    ImageInput,
    UrlField,
    FileInput,
    ArrayInput,
    SelectInput,
    TabbedShowLayout,
    TabbedShowLayoutTabs,
    Tab,
    TextInput,
    NumberInput,
    LongTextInput,
    BooleanInput,
    NullableBooleanInput,
    Filter,
    Pagination
} from 'react-admin';
import {apiUrl} from "../provider";
import React from "react";
import { withStyles } from '@material-ui/core/styles'
import {DateTimeInput} from "react-admin-date-inputs";
import JalaliUtils from "@date-io/jalaali";
import jMoment from "moment-jalaali";
import TimeIcon from '@material-ui/icons/Book';

JalaliUtils.prototype.getStartOfMonth = JalaliUtils.prototype.startOfMonth;
jMoment.loadPersian({ dialect: "persian-modern", usePersianDigits: false });

function temp() {
    return <span>123</span>
}

const styles = {
    inlineBlock: { display: 'inline-flex', marginRight: '1rem' },
    smallerWidth: {width:530}
};
export const BoxCreate =withStyles(styles)(({classes,...props}) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name"/>
            <NumberInput source="type"/>
            <NumberInput source="price" formClassName={classes.inlineBlock}/>
            <NumberInput source="offPrice" formClassName={classes.inlineBlock}/>
            <NumberInput source="coupons"/>
            <DateTimeInput source="endTime" label="Off till"  providerOptions={{ utils:JalaliUtils ,locale:"fa"}}
                           options={{ ampm: false , clearable: true,format: 'jYYYY/jM/jD, HH:mm',showTabs:false , leftArrowIcon:temp ,rightArrowIcon:temp, timeIcon:temp}}/>

        </SimpleForm>
    </Create>
));
export default BoxCreate;