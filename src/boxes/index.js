import BoxCreate from "./BoxCreate";
import BoxEdit from "./BoxEdit";
import BoxList from "./BoxList";
//import BoxShow from "./BoxShow";
import BoxIcon from '@material-ui/icons/ShoppingCart'

export default {
    create:BoxCreate,
    edit:BoxEdit,
    list:BoxList,
   // show:BoxShow,
    icon:BoxIcon
}