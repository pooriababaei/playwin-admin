import AdminCreate from "./AdminCreate";
import AdminEdit from "./AdminEdit";
import AdminList from "./AdminList";
import AdminShow from "./AdminShow";
import AdminIcon from '@material-ui/icons/Face'

export default {
    create:AdminCreate,
    edit:AdminEdit,
    list:AdminList,
    show:AdminShow,
    icon:AdminIcon
}