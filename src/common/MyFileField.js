import React from 'react';
import PropTypes from 'prop-types';
import {apiUrl} from '../provider'
const MyFileField = ({ source, record = {} }) => {
    if (!Array.isArray(record[source])) {
        console.log(apiUrl + record[source]);
        return <a href={apiUrl + record[source]} >{apiUrl + record[source]}</a>

    }

    let links =[];
    for (let i = 0; i < record[source].length; i++) {
        links.push(<a href={apiUrl + record[source]} >{apiUrl + record[source]}</a>)
    }
    return links
};

MyFileField.propTypes = {
    label: PropTypes.string,
    record: PropTypes.object,
    source: PropTypes.string.isRequired,
};
MyFileField.defaultProps = {
    addLabel: true,
};

export default MyFileField;