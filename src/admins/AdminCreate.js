import {
    Labeled,
    List,
    SimpleShowLayout,
    Show,
    Datagrid,
    TextField,
    ImageField,
    DateField,
    NumberField,
    BooleanField,
    FileField,
    TabbedForm,
    FormTab,
    ReferenceField,
    EditButton,
    Edit,
    Create,
    SimpleForm,
    RichTextField,
    DisabledInput,
    SimpleFormIterator,
    ReferenceInput,
    ImageInput,
    UrlField,
    FileInput,
    ArrayInput,
    SelectInput,
    TabbedShowLayout,
    TabbedShowLayoutTabs,
    Tab,
    TextInput,
    NumberInput,
    LongTextInput,
    BooleanInput,
    NullableBooleanInput,
    Filter,
    Pagination
} from 'react-admin';
import {apiUrl} from "../provider";
import React from "react";
import { withStyles } from '@material-ui/core/styles'

const styles = {
    inlineBlock: { display: 'inline-flex', marginRight: '1rem' },
    smallerWidth: {width:530}
};
export const AdminCreate =withStyles(styles)(({classes,...props}) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name"/>
            <TextInput source="username"/>
            <TextInput source="password" type="password"/>
            <TextInput source="email" type="email" formClassName={classes.inlineBlock}/>
            <TextInput source="phone" formClassName={classes.inlineBlock}/>
            <SelectInput source="role" choices={[
                {id: 'superadmin', name: 'Super Admin'},
                {id: 'admin', name: 'Admin'},
            ]}/>
        </SimpleForm>
    </Create>
));
export default AdminCreate;