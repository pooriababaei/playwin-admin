import React, {Component} from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Title, fetchUtils } from 'react-admin';
import {apiUrl} from '../provider';


async function signedUsers(opts) {
    const users = await fetchUtils.fetchJson(`${apiUrl}/users/usersCount`, {
        headers:opts.headers,
        method:'GET'
    });
    return users.json.count;
}
async function totalPurchases(opts) {
    const total = await fetchUtils.fetchJson(`${apiUrl}/boxes/totalPurchasesTillNow`, {
        headers:opts.headers,
        method:'GET'
    });
    return total.json.sum;
}
async function notPaidsNumber(opts) {
    const total = await fetchUtils.fetchJson(`${apiUrl}/payments/notPaidsNumber`, {
        headers:opts.headers,
        method:'GET'
    });
    return total.json.count;
}

export default class Dashboard extends Component {
    state = {
        userCount : 0,
        totalPurchases: 0,
        notPaidsNumber: 0
    };
     componentDidMount() {
         const opts = {
             headers: new Headers({
                 Accept: 'application/json',
                 Authorization: sessionStorage.getItem('Authorization')
                 //  'Content-Type':'multipart/form-data'
             })
         };
        signedUsers(opts).then(res=> {
            this.setState({userCount: res});
        });
         totalPurchases(opts)
             .then(res=> {
                 this.setState({totalPurchases: res});
             });
         notPaidsNumber(opts)
             .then(res=> {
                 this.setState({notPaidsNumber: res});
             });
    }

    render() {
        return <Card style={{width: '25%', backgroundColor: '#ffb9af'}}>
            <Title title="Dashboard"/>
            <CardContent style={{fontSize:'1.3rem', margin:'1rem'}}>
                <h5 style={{display:'inline'}}>Signed Users</h5> : {this.state.userCount}
                <br/>
                <h5 style={{display:'inline'}}>Total Purchases</h5> : {this.state.totalPurchases} Toman
                <br/>
                <h5 style={{display:'inline'}}>Not Paids</h5> : {this.state.notPaidsNumber}
            </CardContent>

        </Card>
    }
}