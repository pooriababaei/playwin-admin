import React from 'react';
import { Route,Router } from 'react-router-dom';
import ReqPassChange from './login/RequestPasswordChangePage';
import passChange from './login/PasswordChangePage';
import Scoreboard from './leagues/scoreboard/Scoreboard'
import {exact} from "prop-types";

export default [
    <Route exact path="/requestChangePass" component={ReqPassChange} noLayout />,
    <Route exact path="/changePass" component={passChange} noLayout />,
    <Route exact path="/scoreboards/:leaguecollectionName" component={Scoreboard} />
];