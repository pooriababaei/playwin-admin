import React from 'react';
import PropTypes from 'prop-types';
import jMoment from 'moment-jalaali'
jMoment.loadPersian({ dialect: "persian-modern", usePersianDigits: true });
const PersianDateTimeField = ({ source, record = {} }) => {
    if (record[source])
    return <span style={{fontSize : '1rem'}}>{jMoment(record[source]).format('jYYYY/jM/jD HH:mm')}</span>;
    return null;
};

PersianDateTimeField.propTypes = {
    label: PropTypes.string,
    record: PropTypes.object,
    source: PropTypes.string.isRequired,
};
PersianDateTimeField.defaultProps = {
    addLabel: true,
};

export default PersianDateTimeField;