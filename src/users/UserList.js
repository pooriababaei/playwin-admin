import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    TextInput,
    Filter,
    Pagination
} from 'react-admin';
import PersianDateTimeField from '../common/PersianDateTimeField'
const UserFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search by username" source="username" alwaysOn />
        <TextInput label="Search by phone number" source="phoneNumber" alwaysOn />

    </Filter>
);
const UserPagination = props => <Pagination rowsPerPageOptions={[10,50,100,1000]} {...props} />

export const UserList = props => (
    <List {...props} pagination={<UserPagination/>} filters={<UserFilter/>} sort={{field: 'coins', order: 'DESC'}} >
        <Datagrid rowClick="show">
            <TextField source="username" sortable={false} />
            <TextField source="phoneNumber" sortable={false}/>
            <TextField source="coins"/>
            <TextField source="loyalties"/>
            <TextField source="coupons"/>
            <PersianDateTimeField source='createdAt' />
        </Datagrid>
    </List>
);
export default UserList
