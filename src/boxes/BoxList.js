import React from 'react'
import {
    List,
    Datagrid,
    TextField,
    NumberField,
    EditButton,
    DeleteButton} from 'react-admin';
import PersianDateTimeField from '../common/PersianDateTimeField'

export const BoxList = props => (
    <List {...props} pagination={null}>
        <Datagrid>
            {/*<TextField source="id" label='Id' hidden sortable={false} />*/}
            <TextField  source="name"/>
            <NumberField  source="price"/>
            <NumberField source="offPrice"/>
            <NumberField source= "type"/>
            <NumberField source ="coupons"/>
            <PersianDateTimeField source="endTime" />
            <EditButton/>
            <DeleteButton/>
        </Datagrid>
    </List>
);
export default BoxList;