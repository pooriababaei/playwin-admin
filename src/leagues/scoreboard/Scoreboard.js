// import PersianDateTimeField from "../../common/PersianDateTimeField";
// import React, {Component} from "react";
// import {
//     List,
//     Datagrid,
//     TextField,
//     NumberField,
//     BooleanField,
//     EditButton,
//     ShowButton,
//     DeleteButton,
//     SelectInput,
//     NullableBooleanInput,
//     Filter,
//     Pagination} from 'react-admin';
//
//
// let fakeProps = {
//     basePath: "/scoreboards",
//     hasCreate: false,
//     hasEdit: false,
//     hasList: true,
//     hasShow: false,
//     history: {},
//     location: { pathname: "/scoreboards", search: "", hash: "", state: undefined },
//     match: { path: "/", url: "/", isExact: true, params: {} },
//     options: {},
//     permissions: null,
//     resource: "scoreboards"
// };
//
// const ScoreboardPagination = props => <Pagination rowsPerPageOptions={[10,50,100,1000]} {...props} />
//
// export const LeagueList = () => {
//    return  <List {...fakeProps} pagination={<ScoreboardPagination/>} >
//         <Datagrid >
//             <TextField source="id" label='Id' hidden />
//             <TextField source="user"/>
//             <TextField source="score"/>
//             <NumberField source="default_opportunities"/>
//             <NumberField source="max_opportunities"/>
//             <NumberField source="kind"/>
//             <PersianDateTimeField source="start_date" />
//             <PersianDateTimeField source="end_date" />
//             <BooleanField source="available"/>
//             <ShowButton/>
//             <EditButton/>
//             <DeleteButton/>
//         </Datagrid>
//     </List>
// };
//
// export default () => (
//    <LeagueList/>
// );
import React, { Component } from 'react';
import { GET_LIST, fetchUtils } from 'react-admin';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import PersianDateTimeField from '../../common/PersianDateTimeField'
import convertDataRequestToHTTP,{getParameterByName} from '../../provider/convertRequests';
import convertResponse from '../../provider/convertResponse'
import {
    List,
    TextField,
    Datagrid,
    NumberField,
    BooleanField
} from 'react-admin';
import RewardButton from '../RewardButton'


export default class Scoreboard extends Component {
    state = {};
    initProps = {
        basePath: "/",
        hasCreate: false,
        hasEdit: false,
        hasList: true,
        hasShow: false,
        location: { pathname: "/scoreboards/" , search: "", hash: "", state: undefined },
        match: { path: "/", url: "/", isExact: true, params: {} },
        options: {
        },
        permissions: null,
        resource: "scoreboards",
        perPage: 10,
        page:1
    };

    componentWillMount() {
        this.unlisten = this.props.history.listen((location, action) => {
            if (location.search) {
                //regex from: https://stackoverflow.com/a/8649003/1501205
                let queryParams = JSON.parse('{"' + decodeURI(location.search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
                let perPage = queryParams.perPage;
                let page = queryParams.page;
                this.initProps.perPage = parseInt(perPage);
                this.initProps.page =  parseInt(page);
                this.initProps.location = location;
                this.setState({'initProps': this.initProps})

            }

        });
    }
    componentWillUnmount() {
        this.unlisten();
    }


    componentDidMount() {
        let parts = window.location.hash.split("/");
        let collectionNameIndex = parts.indexOf('scoreboards') + 1;
        this.initProps.location.pathname += parts[collectionNameIndex];
        this.setState({'initProps': this.initProps});
        const {url,options} = convertDataRequestToHTTP(
            GET_LIST, 'scoreboards', {
                sort: { field: 'date', order: 'DESC' },
                pagination: { page: this.state.initProps ? this.state.initProps.page : 1, perPage: this.state.initProps ? this.state.initProps.perPage : 10 },
            },{
                headers: new Headers({
                    Accept: 'application/json',
                    Authorization: sessionStorage.getItem('Authorization')
                    //  'Content-Type':'multipart/form-data'
                })
            }
        );
        fetchUtils.fetchJson(url,options).then(response => {
            const convertedResponse = convertResponse(response, GET_LIST, 'scoreboards', null);
            this.setState({'scoreboards':  convertedResponse.data});
        });
    }

    render() {
        const {
            initProps
        } = this.state;

        if(!initProps) {
            return false;
        }

        return  <Card> <List {...initProps}  >
            <Datagrid>
                <TextField source="user.username"/>
                <TextField source="score"/>
                <NumberField source="opportunities"/>
                <NumberField source="played"/>
                <PersianDateTimeField source="createdAt" label='First Record' />
                <PersianDateTimeField source="updatedAt" label='Last record'/>
            </Datagrid>
        </List></Card>
    }
}
