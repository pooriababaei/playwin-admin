
import {
    GET_LIST,
    GET_ONE,
    CREATE,
    UPDATE,
    DELETE,
    GET_MANY,
    GET_MANY_REFERENCE,
} from 'react-admin';

const replaceIdWith_IdInArray = (arr) => {
    return arr.map(item => {
        item.id = item._id;
        delete item._id;
        return item;
    });
};
export const replaceIdWith_Id = (item) => {
    item.id = item._id;
    delete item._id;
    return item;
};



export default (response, type, resource, params) => {
    const { headers, json } = response;
    switch (type) {
        case GET_LIST:
        case GET_MANY_REFERENCE:
            if (!headers.has('x-total-count')) {
                throw new Error(
                    'The X-Total-Count header is missing in the HTTP Response. The jsonServer Data Provider expects responses for lists of resources to contain this header with the total number of results to build the pagination. If you are using CORS, did you declare X-Total-Count in the Access-Control-Expose-Headers header?'
                );
            }

            return {
                data: replaceIdWith_IdInArray(json),
                total: parseInt(
                    headers
                        .get('x-total-count')
                        .split('/')
                        .pop(),
                    10
                ),
            };
        case DELETE:
        case CREATE:
            return { data: { ...params.data, id: json._id } };
        default:
            return { data: replaceIdWith_Id(json) };
    }
};