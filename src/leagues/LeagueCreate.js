import {
    Labeled,
    List,
    SimpleShowLayout,
    Show,
    Datagrid,
    TextField,
    ImageField,
    DateField,
    NumberField,
    BooleanField,
    FileField,
    TabbedForm,
    FormTab,
    ReferenceField,
    EditButton,
    Edit,
    Create,
    SimpleForm,
    RichTextField,
    DisabledInput,
    SimpleFormIterator,
    ReferenceInput,
    ImageInput,
    UrlField,
    FileInput,
    ArrayInput,
    SelectInput,
    TabbedShowLayout,
    TabbedShowLayoutTabs,
    Tab,
    TextInput,
    NumberInput,
    LongTextInput,
    BooleanInput,
    NullableBooleanInput,
    Filter,
    Pagination
} from 'react-admin';

import RichTextInput from "ra-input-rich-text";
import {DateTimeInput} from "react-admin-date-inputs";
import {apiUrl} from "../provider";
import JalaliUtils from "@date-io/jalaali";
import jMoment from "moment-jalaali";
import React from "react";
import TimeIcon from '@material-ui/icons/Book';

import { withStyles } from '@material-ui/core/styles'
JalaliUtils.prototype.getStartOfMonth = JalaliUtils.prototype.startOfMonth;
jMoment.loadPersian({ dialect: "persian-modern", usePersianDigits: false });

const styles = {
    inlineBlock: { display: 'inline-flex', marginRight: '1rem' },
    smallerWidth: {width:530}
};
function temp() {
    return <span>123</span>
}
export const LeagueCreate =withStyles(styles)(({classes,...props}) => (
    <Create {...props}>
        <TabbedForm>
            <FormTab label="Info">
                <TextInput source="name" required/>
                <TextInput source="collectionName" required/>
                <br/>
                <RichTextInput source="description" formClassName={classes.smallerWidth}/>
                <br/>
                <NumberInput source="coinsReward" formClassName={classes.inlineBlock} required/>
                <NumberInput source="leadersNumber" formClassName={classes.inlineBlock} required/>
                <br/>
                <NumberInput source="loyaltiesReward" formClassName={classes.inlineBlock}/>
                <NumberInput source="loyaltyGivens" formClassName={classes.inlineBlock}/>
                <br/>
                <NumberInput source="defaultOpportunities" formClassName={classes.inlineBlock} required/>
                <NumberInput source="maxOpportunities" formClassName={classes.inlineBlock} required/>
                <SelectInput source="kind" choices={[
                    {id: 1, name: 'Oppo = play (1)'},
                    {id: 2, name: 'Oppo = New Record (2)'},
                ]} required/>
                <br/>
                <BooleanInput source="available" />
                <BooleanInput source="gameHidden" />
                <br/>
                <DateTimeInput source="startTime" label="Start time" required  providerOptions={{ utils:JalaliUtils ,locale:"fa"}}
                               options={{ ampm: false , clearable: true,format: 'jYYYY/jM/jD, HH:mm',showTabs:false , leftArrowIcon:temp ,rightArrowIcon:temp, timeIcon:temp}}
                               formClassName={classes.inlineBlock}/>
                <DateTimeInput source="endTime" label="End time" required  providerOptions={{ utils:JalaliUtils ,locale:"fa"}}
                               options={{ ampm: false ,clearable: true, format: 'jYYYY/jM/jD, HH:mm',showTabs:false , leftArrowIcon:temp ,rightArrowIcon:temp, timeIcon:temp }}
                               formClassName={classes.inlineBlock}/>
                <br/>
                <TextInput source="html" label="Html File Path"/>
                <TextInput source ="color" label ="Color" formClassName={classes.inlineBlock} resettable/>
            </FormTab>

            <FormTab label = "Files">
                <ImageInput source="mainImage" label="Main Image" accept="image/*">
                    <ImageField  source="mainImage"/>
                </ImageInput>
                <ImageInput source="images" label="Images" multiple accept="image/*">
                    <ImageField source="images" />
                </ImageInput>

                <FileInput source="game" label="Game File" placeholder={<p>Drop your file here</p>}   >
                    <FileField  source="gameZip"/>
                </FileInput>
            </FormTab>
        </TabbedForm>
    </Create>
));
export default LeagueCreate;