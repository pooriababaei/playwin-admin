import LeagueCreate from "./LeagueCreate";
import LeagueEdit from "./LeagueEdit";
import LeagueShow from "./LeagueShow";
import LeagueList from "./LeagueList";
import LeagueIcon from '@material-ui/icons/LocalPlay'


export default  {
    create: LeagueCreate,
    edit:LeagueEdit,
    show:LeagueShow,
    list:LeagueList,
    icon:LeagueIcon

}