import React from 'react';
import { Link } from 'react-router-dom';
import ScoreboardIcon from '@material-ui/icons/InsertChartOutlinedOutlined';
import { withStyles } from '@material-ui/core/styles';
import { Button } from 'react-admin';

const styles = {
    button: {
       // marginTop: '1em',
    }
};

const ScoreboardButton = ({ classes, record }) => {
    let start_time = new Date(record.startTime).getTime();
   return <Button
        className={classes.button}
        variant="text"
        component={Link}
        to={`/scoreboards/${record.collectionName}`}
        label="Scoreboard"
        title="Scoreboard"
        disabled={start_time > Date.now()}
    >
        <ScoreboardIcon/>
    </Button>
};

export default withStyles(styles)(ScoreboardButton);
