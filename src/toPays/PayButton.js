// in src/comments/RewardButton.js
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { showNotification,UPDATE,withDataProvider } from 'react-admin';
import { push } from 'react-router-redux';
import PayIcon from '@material-ui/icons/Payment';

class PayButton extends Component {
    handleClick = () => {
        const {dataProvider, dispatch, record} = this.props;

        dataProvider(UPDATE, 'payments', {id: record.id , data: {paid : true , payTime:Date.now()}}, {
            onSuccess: {
                notification: {body: 'Paid Successfully', level: 'info'},
               // redirectTo: '/payments',
            },
            onError: {
                notification: {body: 'Error', level: 'warning'},
               // redirectTo: '/payments',
            }
        })
    }

render() {
        const record = this.props.record;
        return <Button   variant={"flat"} style={{color: record.paid !== true ? '#bc6a00': 'rgba(147,147,147,0.64)' }} disabled={record.paid}  onClick={this.handleClick}><PayIcon style={{marginRight:'0.2rem'}}/>Pay</Button>
    }
}
PayButton.propTypes = {
    dataProvider: PropTypes.func.isRequired,
    record: PropTypes.object,
};

export default withDataProvider(PayButton);