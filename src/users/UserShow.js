import PersianDateTimeField from "../common/PersianDateTimeField";
import {
    Labeled,
    List,
    SimpleShowLayout,
    Show,
    Datagrid,
    TextField,
    ImageField,
    DateField,
    NumberField,
    BooleanField,
    FileField,
    ReferenceField,
    EditButton,
    Edit,
    Create,
    SimpleForm,
    DisabledInput,
    ReferenceInput,
    ImageInput,
    DateInput,
    FileInput,
    SelectInput,
    TextInput,
    NumberInput,
    LongTextInput,
    BooleanInput,
    NullableBooleanInput,
    Filter,
    Pagination
} from 'react-admin';
import React from "react";
const UserTitle = ({record}) => {
    return <span>User {record ? `"${record.username}"` : ''}</span>;
};
export const UserShow = props => (
    <Show title={<UserTitle/>} {...props}>

        <SimpleShowLayout>
            <TextField source="id" />
            <TextField source="username" />
            <TextField source="phoneNumber"/>
            <TextField source="coins"/>
            <TextField source="loyalties"/>
            <TextField source="coupons"/>
            <PersianDateTimeField source='createdAt' />
            <TextField source="avatar"/>
        </SimpleShowLayout>
    </Show>
);