import { withRouter } from 'react-router-dom';
import { Authenticated, Title } from 'react-admin';
import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

const RequestPasswordChangePage = ({ location }) => (
    <Authenticated authParams={{ resource: 'RequestPasswordChangePage' }} location={location}>
        <div>
             <Card>
            <Title title="My Page" />
            <CardContent>
                Card Content
            </CardContent>
        </Card>
        </div>
    </Authenticated>
);

export default withRouter(RequestPasswordChangePage);