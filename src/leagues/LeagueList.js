import PersianDateTimeField from "../common/PersianDateTimeField";
import React from "react";
import {
    List,
    Datagrid,
    TextField,
    NumberField,
    BooleanField,
    EditButton,
    ShowButton,
    DeleteButton,
    SelectInput,
    NullableBooleanInput,
    Filter} from 'react-admin';
import ScoreboardButton from "./ScoreboardButton";
import RewardButton from "./RewardButton";

const LeagueFilter = (props) => (
    <Filter {...props}>
        <SelectInput label="Kind" source="kind" choices={[
            {id: '1', name: 'Oppo = play'},
            {id: '2', name: 'Oppo = New Record'},
        ]} alwaysOn/>
        <SelectInput label="Time" source="running" choices={[
            {id: '1', name: 'Running'},
            {id: '2', name: 'Future'},
            {id: '3', name: 'Past'}
        ]} alwaysOn/>
        <NullableBooleanInput label="Available" source="available" alwaysOn/>

    </Filter>
);

export const LeagueList = props => (
    <List {...props} pagination={null}  filters={<LeagueFilter/>} sort={{field: 'startTime', order: 'ASC'}}>

        <Datagrid >
            {/*<TextField source="id" label='Id' hidden />*/}
            <TextField source="name"/>
            <TextField source="collectionName"/>
            <NumberField source="kind"/>
            <PersianDateTimeField source="startTime" />
            <PersianDateTimeField source="endTime" />
            <BooleanField source="available"/>
            <NumberField source="playersNumber"/>
            <BooleanField source = 'rewarded'/>
            <ScoreboardButton/>
            <ShowButton/>
            <EditButton/>
            <DeleteButton/>
            <RewardButton/>
        </Datagrid>
    </List>
);
export default LeagueList;