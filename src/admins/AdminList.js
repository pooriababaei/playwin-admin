import React from 'react'
import {
    List,
    Datagrid,
    TextField,
    EditButton} from 'react-admin';

export const AdminList = props => (
    <List {...props} pagination={null} >
        <Datagrid rowClick="show">
            <TextField source="id" label='Id' hidden sortable={false} />
            <TextField source="name" sortable={false}/>
            <TextField source="username" sortable={false}/>
            <TextField source="phone" sortable={false}/>
            <TextField source="email" sortable={false}/>
            <EditButton/>
        </Datagrid>
    </List>
);
export default AdminList;