import React from 'react';
import PropTypes from 'prop-types';
function renderKind (kind) {
    if (kind == null)
         return null;
    if(kind == 1)
        return 'Oppo = Play (1)';
    else if( kind == 2)
        return 'Oppo = New Record (2)';
}
const KindField = ({ source, record = {},  }) => <span>{renderKind(record[source])}</span>;

KindField.propTypes = {
    label: PropTypes.string,
    record: PropTypes.object,
    source: PropTypes.string.isRequired,
};
KindField.defaultProps = {
    addLabel: true,
};

export default KindField;