import {fetchUtils, UPDATE_MANY,DELETE_MANY,UPDATE,CREATE,GET_LIST} from 'react-admin'
import convertDataRequestToHTTP from './convertRequests'
import convertHTTPResponse  from './convertResponse'
export const apiUrl = "https://api.playwin.ir";
export default (httpClient = fetchUtils.fetchJson) => {

    return (type, resource, params) => {
        const opts = {
            headers: new Headers({
                Accept: 'application/json',
                Authorization: sessionStorage.getItem('Authorization')
                //  'Content-Type':'multipart/form-data'
            })
    };

        if (resource === 'leagues' || 'games') {
            let formData  = new FormData();

            for(let name in params.data) {
                if(name !== 'mainImage' && name !== 'game' && name !== 'images')
                    formData.append(name, params.data[name]);
                else {
                    formData.append(name, params.data[name].rawFile);
                }

            }
            if(type === CREATE) {

                return httpClient(`${apiUrl}/${resource}`, {
                    headers:opts.headers,
                    method:'POST',
                    body: formData,
                }).then(response => ({
                    data: {...response.json, id: response.json._id}
                }));
            }
            else if(type === UPDATE) {
                if (resource === 'rewardLeagueWinners') {
                    return httpClient(`${apiUrl}/currencies/${resource}/${params.collectionName}`, {
                        headers:opts.headers,
                        method:'PUT',
                    }).then(response => ({
                        data: {...response.json, id: response.json._id}
                    }));
                }
                // if (resource === 'payToPay') {
                //     return httpClient(`${apiUrl}/payments/${resource}/${params.id}`, {
                //         headers:opts.headers,
                //         body: JSON.stringify({paid: true , payTime : Date.now()})
                //         method:'PUT',
                //     }).then(response => ({
                //         data: {...response.json, id: response.json._id}
                //     }));
                // }
                // return httpClient(`${apiUrl}/${resource}/${params.id}`, {
                //     headers:opts.headers,
                //     method:'PUT',
                //     body: formData,
                // }).then(response => ({
                //     data: {...response.json, id: response.json._id}
                // }));
            }
        }

        // json-server doesn't handle filters on UPDATE route, so we fallback to calling UPDATE n times instead
        if (type === UPDATE_MANY) {
            return Promise.all(
                params.ids.map(id =>
                    httpClient(`${apiUrl}/${resource}/${id}`, {
                        headers:opts.headers,
                        method: 'PUT',
                        body: JSON.stringify(params.data),
                    })
                )
            ).then(responses => ({
                data: responses.map(response => response.json),
            }));
        }
        // json-server doesn't handle filters on DELETE route, so we fallback to calling DELETE n times instead
        if (type === DELETE_MANY) {
            return Promise.all(
                params.ids.map(id =>
                    httpClient(`${apiUrl}/${resource}/${id}`, {
                        headers:opts.headers,
                        method: 'DELETE',
                    })
                )
            ).then(responses => ({
                data: responses.map(response => response.json),
            }));
        }

        const {url,options} = convertDataRequestToHTTP(
            type,
            resource,
            params,
            opts
        );
        return httpClient(url, options).then(response =>
            convertHTTPResponse(response, type, resource, params)
        );
    };

}
