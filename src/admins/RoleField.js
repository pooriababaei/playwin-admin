import React from 'react';
import PropTypes from 'prop-types';
function renderRole (role) {
    if (role == null)
         return null;
    if(role === 'superadmin')
        return 'Super Admin';
    else if( role === 'admin')
        return 'Admin';
}
const RoleField = ({ source, record = {},  }) => <span>{renderRole(record[source])}</span>;

RoleField.propTypes = {
    label: PropTypes.string,
    record: PropTypes.object,
    source: PropTypes.string.isRequired,
};
RoleField.defaultProps = {
    addLabel: true,
};

export default RoleField;