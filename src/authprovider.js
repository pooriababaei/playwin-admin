import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK,AUTH_GET_PERMISSIONS } from 'react-admin';
import decodeJwt from 'jwt-decode';
import jwt from 'jsonwebtoken'
import {apiUrl} from '../src/provider/index'
const KEY = 'somerandomkeyfortimestampencoding123#*';
export default (type, params) => {
    // called when the user attempts to log in
    if (type === AUTH_LOGIN) {
        const { username, password } = params;
        const request = new Request(`${apiUrl}/admins/login`, {
            method: 'POST',
            body: JSON.stringify({ username, password }),
            headers: new Headers({ 'Content-Type': 'application/json'}),
        });
        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(({ token }) => {
                const ts = new Date().getTime();
                const signedTimeStamp = jwt.sign({timestamp: ts},KEY,{expiresIn:'0.5h'});
                sessionStorage.setItem('TimeStamp', signedTimeStamp);
                sessionStorage.setItem('Authorization','Bearer ' + token);
            });
    }
    // called when the user clicks on the logout button
    if (type === AUTH_LOGOUT) {
        sessionStorage.removeItem('Authorization');
        sessionStorage.removeItem('TimeStamp');
        return Promise.resolve();
    }
    // called when the API returns an error
    if (type === AUTH_ERROR) {
        const { status } = params;
        if (status === 401 || status === 403) {
            sessionStorage.removeItem('Authorization');
            sessionStorage.removeItem('TimeStamp');
            return Promise.reject();
        }
        return Promise.resolve();
    }
    // called when the user navigates to a new location
    if (type === AUTH_CHECK) {
        // const { resource } = params;
        // if (resource === 'posts') {
        //     // check credentials for the posts resource
        // }

        if(sessionStorage.getItem('Authorization')) {
            try {
                const decoded = jwt.verify(sessionStorage.getItem('TimeStamp'),KEY);
                if(decoded.timestamp) {
                    const ts = new Date().getTime();
                    const newSignedTimeStamp = jwt.sign({timestamp:ts},KEY,{expiresIn:'0.5h'});
                    sessionStorage.setItem('TimeStamp', newSignedTimeStamp);
                    return Promise.resolve();
                }
            }catch (e) {
                return Promise.reject();
            }
        }
        else
            return Promise.reject();
    }
    if (type === AUTH_GET_PERMISSIONS) {
        const resource = {params};
        const role = decodeJwt(sessionStorage.getItem('Authorization')).role;
        if(resource === 'admins') {
            return role ==='superadmin' ? Promise.resolve(role) : Promise.reject();
        }
        return role ? Promise.resolve(role) : Promise.reject();
    }

    return Promise.reject('Unknown method');
};
// export default (type, params) => {
//     // called when the user attempts to log in
//
//     return Promise.resolve();
// };