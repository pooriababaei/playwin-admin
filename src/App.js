import React from 'react';
import DashBoard from './dashboard';
import AuthProvider from './authprovider'
import {Admin, Resource,fetchUtils, Rout} from 'react-admin';
import MyLoginPage from './login/Login';
import login from './login/Login'
import customRoutes from './customRoutes';
import simpleRestProvider  from './provider';
import users from './users';
import leagues from './leagues';
import boxes from './boxes'
import games from './games'
import admins from './admins'
import toPays from './toPays'

const dataProvider = simpleRestProvider ();
const App = () => {
    return <Admin  dataProvider={dataProvider} dashboard={DashBoard} authProvider={AuthProvider} customRoutes={customRoutes} >
        {permissions => [
            <Resource name="users"   {... users} />,
            <Resource name="leagues"  {...leagues} />,
            <Resource name="boxes"  {...boxes} />,
            <Resource name="games"  {...games} />,
            <Resource name="payments"  {...toPays} />,
            <Resource name="scoreboards"  />,
            permissions === 'superadmin'
                ? <Resource name="admins" {...admins}/>
                : null,
        ]}

    </Admin>
}

export default App;