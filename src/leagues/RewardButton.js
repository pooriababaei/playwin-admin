// in src/comments/RewardButton.js
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import { showNotification,UPDATE,withDataProvider } from 'react-admin';
import { push } from 'react-router-redux';
import RewardIcon from '@material-ui/icons/Redeem';

function active(startTime,endTime,rewarded) {
    let start = new Date(startTime).getTime();
    let end = new Date(endTime).getTime();
    return !(rewarded || start > Date.now() || end > Date.now());

;}

class ApproveButton extends Component {


    handleClick = () => {
        const {dataProvider, dispatch, record} = this.props;

        dataProvider(UPDATE, 'rewardLeagueWinners', {collectionName: record.collectionName}, {
            onSuccess: {
                notification: {body: 'Winners Rewarded', level: 'info'},
              //  redirectTo: '/leagues',
            },
            onError: {
                notification: {body: 'Error', level: 'warning'},
               // redirectTo: '/leagues',
            }
        })
    }

render() {
        const record = this.props.record;
    return <Button   variant={"flat"} style={{color: active(record.startTime,record.endTime,record.rewarded) ? '#15bc5b': 'rgba(147,147,147,0.64)' , fontSize:'.6rem'}} disabled={ !active(record.startTime,record.endTime,record.rewarded)}  onClick={this.handleClick}><RewardIcon />Reward Winners</Button>
    }
}
ApproveButton.propTypes = {
    dataProvider: PropTypes.func.isRequired,
    record: PropTypes.object,
};

export default withDataProvider(ApproveButton);