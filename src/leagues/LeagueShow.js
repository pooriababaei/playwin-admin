import {withStyles} from "@material-ui/core/styles";
import KindField from "./KindField";
import PersianDateTimeField from "../common/PersianDateTimeField";
import {apiUrl} from "../provider";
import React from "react";
import {
    Show,
    TextField,
    ImageField,
    NumberField,
    BooleanField,
    FileField,
    RichTextField,
    UrlField,
    TabbedShowLayout,
    TabbedShowLayoutTabs,
    ArrayField,
    Tab
} from 'react-admin';
import ScoreboardButton from "./ScoreboardButton";
import RewardButton from "./RewardButton";
import MyImageField from "../common/MyImageField";
import MyFileField from "../common/MyFileField";

const styles = {
    inlineBlock: { display: 'inline-flex', marginRight: '1rem' },
    smallerWidth: {width:530}
};
const LeagueTitle = ({record}) => {
    return <span>League {record ? `"${record.name}"` : ''}</span>;
};
export const LeagueShow = withStyles(styles)(({classes,...props}) => {
   return <Show title={<LeagueTitle/>} {...props}>
        <TabbedShowLayout tabs={<TabbedShowLayoutTabs scrollable={true}/>}>
            <Tab label = "info">
                <TextField source="id" />
                <TextField source="name" className={classes.inlineBlock}/>
                <TextField source="collectionName" className={classes.inlineBlock}/>
                <RichTextField source="description"/>
                <NumberField source="leadersNumber" className={classes.inlineBlock}/>
                <NumberField source="coinsReward" className={classes.inlineBlock}/>
                <br/>
                <NumberField source="loyaltyGivens" className={classes.inlineBlock}/>
                <NumberField source="loyaltiesReward" className={classes.inlineBlock}/>
                <br/>
                <NumberField source="defaultOpportunities" className={classes.inlineBlock}/>
                <NumberField source="maxOpportunities" className={classes.inlineBlock}/>
                <KindField source="kind" />
                <BooleanField source="rewarded"/>
                <BooleanField source="available" className={classes.inlineBlock}/>
                <BooleanField source="gameHidden" className={classes.inlineBlock}/>
                <PersianDateTimeField source="startTime"  className={classes.inlineBlock}/>
                <PersianDateTimeField source="endTime"  className={classes.inlineBlock}/>
                <TextField source = "html" label="Html File Relative Path"/>
                <TextField source = "color" label ="Color" ClassName={classes.inlineBlock} />
                <MyFileField source = "game" title="Game"/>
                <br/>
            </Tab>
            <Tab label = "Files">
                <MyImageField source="mainImage"  title='Main Image'/>
                <MyImageField source="images"  title="Images"/>
                <MyFileField source ="gameZip" title="Game Zip"/>
            </Tab>
        </TabbedShowLayout>
    </Show>
})
export default LeagueShow;