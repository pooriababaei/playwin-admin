import React from 'react';
import PropTypes from 'prop-types';
import {apiUrl} from '../provider'
const MyImageField = ({ source, record = {} }) => {
    if (!Array.isArray(record[source])) {
        return <img src={apiUrl + record[source]} width={'200px'} alt={'Not Found'}/>
    }

    let images =[];
    for (let i = 0; i < record[source].length; i++) {
        images.push(<img src={apiUrl + record[source][i]} width={'200px'}  alt={'Not Found'}/>)
    }
    return images
};

MyImageField.propTypes = {
    label: PropTypes.string,
    record: PropTypes.object,
    source: PropTypes.string.isRequired,
};
MyImageField.defaultProps = {
    addLabel: true,
};

export default MyImageField;