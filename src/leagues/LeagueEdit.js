import {
    Labeled,
    List,
    SimpleShowLayout,
    Show,
    Datagrid,
    TextField,
    ImageField,
    DateField,
    NumberField,
    BooleanField,
    FileField,
    TabbedForm,
    FormTab,
    ReferenceField,
    EditButton,
    Edit,
    Create,
    SimpleForm,
    RichTextField,
    DisabledInput,
    SimpleFormIterator,
    ReferenceInput,
    ImageInput,
    UrlField,
    FileInput,
    ArrayInput,
    SelectInput,
    TabbedShowLayout,
    TabbedShowLayoutTabs,
    Tab,
    TextInput,
    NumberInput,
    LongTextInput,
    BooleanInput,
    NullableBooleanInput,
    Filter,
    Pagination
} from 'react-admin';
import TimeIcon from '@material-ui/icons/Book';
import {withStyles} from "@material-ui/core/styles";
import RichTextInput from "ra-input-rich-text";
import {DateTimeInput} from "react-admin-date-inputs";
import {apiUrl} from "../provider";
import React from "react";
import JalaliUtils from "@date-io/jalaali";
import jMoment from "moment-jalaali";
JalaliUtils.prototype.getStartOfMonth = JalaliUtils.prototype.startOfMonth;
jMoment.loadPersian({ dialect: "persian-modern", usePersianDigits: true });

const styles = {
    inlineBlock: { display: 'inline-flex', marginRight: '1rem' },
    smallerWidth: {width:530}
};


function temp() {
    return ({TimeIcon})
}
const LeagueTitle = ({record}) => {
    return <span>League {record ? `"${record.name}"` : ''}</span>;
};

export const LeagueEdit =withStyles(styles)(({classes,...props}) => (
    <Edit title={<LeagueTitle/>} {...props} >
        <TabbedForm>
            <FormTab label = "Info">
                <DisabledInput source="id" label='Id'  />
                <DisabledInput source="name" formClassName={classes.inlineBlock}/>
                <DisabledInput source="collectionName" formClassName={classes.inlineBlock}/>
                <br/>
                <RichTextInput source="description" formClassName={classes.smallerWidth}/>
                <br/>
                <NumberInput source="leadersNumber" required formClassName={classes.inlineBlock}/>
                <NumberInput source="coinsReward" required formClassName={classes.inlineBlock}/>
                <br/>
                <NumberInput source="loyaltyGivens" formClassName={classes.inlineBlock}/>
                <NumberInput source="loyaltiesReward" formClassName={classes.inlineBlock}/>
                <br/>
                <DisabledInput source="defaultOpportunities" formClassName={classes.inlineBlock}/>
                <NumberInput source="maxOpportuniies" formClassName={classes.inlineBlock}/>
                <SelectInput source="kind" required choices={[
                    {id: 1, name: 'Oppo = play (1)'},
                    {id: 2, name: 'Oppo = New Record (2)'},
                ]}/>
                <br/>
                <BooleanInput source="available" formClassName={classes.inlineBlock}/>
                <BooleanInput source="gameHidden" formClassName={classes.inlineBlock}/>
                <br/>
                <DateTimeInput source="startTime" label="Start time" required providerOptions={{ utils:JalaliUtils ,locale:"fa"}}
                               options={{ ampm: false ,clearable: true,format: 'jYYYY/jM/jD, HH:mm',showTabs:false , leftArrowIcon:temp ,rightArrowIcon:temp, timeIcon:temp}}
                               formClassName={classes.inlineBlock}/>
                <DateTimeInput source="endTime" label="End time" required providerOptions={{ utils:JalaliUtils ,locale:"fa"}}
                               options={{ ampm: false ,clearable: true,format: 'jYYYY/jM/jD, HH:mm',showTabs:false, leftArrowIcon:temp ,rightArrowIcon:temp, timeIcon:temp}}
                               formClassName={classes.inlineBlock}/>
                <br/>
                <TextInput source="html file relative path" label="Html File Path"/>
                <TextInput source ="color" label ="Color" formClassName={classes.inlineBlock} resettable/>
            </FormTab>

            <FormTab label = "Files">
                <ImageInput source="mainImage" label="Main Image" accept="image/*">
                    <ImageField  source="mainImage"/>
                </ImageInput>
                <ImageInput source="images" label="Images" multiple accept="image/*">
                    <ImageField source="images"/>
                </ImageInput>

                <FileInput source="game" label="Game File" placeholder={<p>Drop your file here</p>}   >
                    <FileField  source="gameZip"/>
                </FileInput>
            </FormTab>
        </TabbedForm>
    </Edit>
));
export default LeagueEdit;
