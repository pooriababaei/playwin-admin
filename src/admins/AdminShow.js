import {
    Labeled,
    List,
    SimpleShowLayout,
    Show,
    Datagrid,
    TextField,
    ImageField,
    DateField,
    NumberField,
    BooleanField,
    FileField,
    TabbedForm,
    FormTab,
    ReferenceField,
    EditButton,
    Edit,
    Create,
    SimpleForm,
    RichTextField,
    DisabledInput,
    SimpleFormIterator,
    ReferenceInput,
    ImageInput,
    UrlField,
    FileInput,
    ArrayInput,
    SelectInput,
    TabbedShowLayout,
    TabbedShowLayoutTabs,
    Tab,
    TextInput,
    NumberInput,
    LongTextInput,
    BooleanInput,
    NullableBooleanInput,
    Filter,
    Pagination
} from 'react-admin';
import {apiUrl} from "../provider";
import React from "react";
import { withStyles } from '@material-ui/core/styles'
import RoleField from './RoleField'

const styles = {
    inlineBlock: { display: 'inline-flex', marginRight: '1rem' },
    smallerWidth: {width:530}
};
const AdminTitle = ({record}) => {
    return <span>Admin {record ? `"${record.name}"` : ''}</span>;
};
export const AdminShow =withStyles(styles)(({classes,...props}) => (
    <Show title={<AdminTitle/>} {...props}>
        <SimpleShowLayout>
            <TextField source="id" label='Id' />
            <TextField source="name"/>
            <TextField source="username"/>
            <TextField source="email" className={classes.inlineBlock}/>
            <TextField source="phone" className={classes.inlineBlock}/>
            <RoleField source="role"/>
        </SimpleShowLayout>
    </Show>
));
export default AdminShow;