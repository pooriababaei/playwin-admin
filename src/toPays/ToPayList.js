import PersianDateTimeField from "../common/PersianDateTimeField";
import Button from '@material-ui/core/Button';
import { CardActions, CreateButton, ExportButton, RefreshButton } from 'react-admin';
import React, {Fragment} from "react";
import {
    List,
    Datagrid,
    TextField,
    NumberField,
    BooleanField,
    SelectInput,
    BulkDeleteButton,
    Pagination,
    NullableBooleanInput,
    Filter} from 'react-admin';
import PayButton from "./PayButton";
import PaySelectedIdsButton from "./PaySelectedIdsButton";

const PaymentFilter = (props) => (
    <Filter {...props}>
        <NullableBooleanInput label="Paid" source="paid" alwaysOn/>
    </Filter>
);

// const PaymentBulkActionButtons = props => (
//     <Fragment>
//         <PaySelectedIdsButton label="Pay selected payments" {...props} />
//         {/* Add the default bulk delete action */}
//         {/*<BulkDeleteButton {...props} />*/}
//     </Fragment>
// );
//
// const PostActions = ({
//                          bulkActions,
//                          basePath,
//                          currentSort,
//                          displayedFilters,
//                          exporter,
//                          filters,
//                          filterValues,
//                          onUnselectItems,
//                          resource,
//                          selectedIds,
//                          showFilter,
//                          total
//                      }) => (
//     <CardActions>
//         {bulkActions && React.cloneElement(bulkActions, {
//             basePath,
//             filterValues,
//             resource,
//             selectedIds,
//             onUnselectItems,
//         })}
//         {filters && React.cloneElement(filters, {
//             resource,
//             showFilter,
//             displayedFilters,
//             filterValues,
//             context: 'button',
//         }) }
//         <ExportButton
//             disabled={total === 0}
//             resource={resource}
//             sort={currentSort}
//             filter={filterValues}
//             exporter={exporter}
//         />
//         {/* Add your custom actions */}
//     </CardActions>
// );

const PaymentPagination = props => <Pagination rowsPerPageOptions={[10,50,100,1000]} {...props} />;

export const ToPayList = props => (
    <List {...props} pagination={<PaymentPagination/>}  filters={<PaymentFilter/>}
                     filterDefaultValues={{ paid: false }} sort={{field: 'createdAt', order: 'DESC'}}  >

        <Datagrid >
            <TextField source="paymentNumber"/>
            <TextField source="user.username" label="Username"/>
            <TextField source="user.phoneNumber" label="Phone Number"/>
            <TextField source="user.account"/>
            <TextField source="amount"/>
            <PersianDateTimeField source="createdAt" />
            <PersianDateTimeField source="payTime" />
            <BooleanField source = 'paid'/>
            <PayButton/>
        </Datagrid>
    </List>
);

export default ToPayList;