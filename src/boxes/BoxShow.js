// import {
//     Labeled,
//     List,
//     SimpleShowLayout,
//     Show,
//     Datagrid,
//     TextField,
//     ImageField,
//     DateField,
//     NumberField,
//     BooleanField,
//     FileField,
//     TabbedForm,
//     FormTab,
//     ReferenceField,
//     EditButton,
//     Edit,
//     Create,
//     SimpleForm,
//     RichTextField,
//     DisabledInput,
//     SimpleFormIterator,
//     ReferenceInput,
//     ImageInput,
//     UrlField,
//     FileInput,
//     ArrayInput,
//     SelectInput,
//     TabbedShowLayout,
//     TabbedShowLayoutTabs,
//     Tab,
//     TextInput,
//     NumberInput,
//     LongTextInput,
//     BooleanInput,
//     NullableBooleanInput,
//     Filter,
//     Pagination
// } from 'react-admin';
// import {apiUrl} from "../provider";
// import React from "react";
// import { withStyles } from '@material-ui/core/styles'
// import MyImageField from "../common/MyImageField";
//
// const styles = {
//     inlineBlock: { display: 'inline-flex', marginRight: '1rem' },
//     smallerWidth: {width:530}
// };
// const BoxTitle = ({record}) => {
//     return <span>Box {record ? `"${record.name}"` : ''}</span>;
// };
// export const BoxShow =withStyles(styles)(({classes,...props}) => (
//     <Show title={<BoxTitle/>} {...props}>
//         <SimpleShowLayout>
//             <TextField source="name"/>
//             <NumberField source="price" className={classes.inlineBlock}/>
//             <NumberField source="offPrice" className={classes.inlineBlock}/>
//             <MyImageField  source="image" title = "Image" />
//         </SimpleShowLayout>
//     </Show>
// ));
// export default BoxShow;